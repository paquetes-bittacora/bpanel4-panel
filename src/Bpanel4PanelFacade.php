<?php

namespace Bittacora\Bpanel4Panel;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Bpanel4Panel\Bpanel4Panel
 */
class Bpanel4PanelFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'bpanel4-panel';
    }
}
