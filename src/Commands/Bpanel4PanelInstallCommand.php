<?php

namespace Bittacora\Bpanel4Panel\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Bpanel4PanelInstallCommand extends Command
{
    public $signature = 'bpanel4-panel:install';

    public $description = 'Instala el paquete base para el panel de control';

    public function handle(): void
    {
        $this->comment('Dando permisos al rol de administración');
        $permissions = ['index'];
        $adminRole = Role::findOrCreate('admin');
        foreach($permissions as $permission){
            $permission = Permission::firstOrCreate(['name' => 'bpanel.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }

        $this->comment('Hecho');
    }
}
