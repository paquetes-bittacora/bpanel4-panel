<?php

namespace Bittacora\Bpanel4Panel\Commands;

use Illuminate\Console\Command;

class Bpanel4PanelCommand extends Command
{
    public $signature = 'bpanel4-panel';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
