<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4Panel\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;

class BpanelController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('bpanel.index');
        return view('bpanel4::index');
    }
}
