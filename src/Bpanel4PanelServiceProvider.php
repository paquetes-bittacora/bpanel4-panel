<?php

namespace Bittacora\Bpanel4Panel;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Bpanel4Panel\Commands\Bpanel4PanelInstallCommand;

class Bpanel4PanelServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('bpanel4-panel')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_bpanel4-panel_table')
            ->hasCommand(Bpanel4PanelInstallCommand::class);
    }

    public function register(): void
    {
        parent::register();
        $this->loadJsonTranslationsFrom(__DIR__ . '/../../../rappasoft/laravel-livewire-tables/resources/lang/');
    }

    public function boot(): void
    {
        $this->commands(Bpanel4PanelInstallCommand::class);
        $this->setPublishableFiles();
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'bpanel4');
    }

    private function setPublishableFiles()
    {
        $this->publishes([
            __DIR__.'/../resources/assets' => resource_path('bpanel4/assets'),
        ], 'bpanel4-panel');
    }
}
