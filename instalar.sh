#!/usr/bin/env bash

composer require bittacora/bpanel4-panel

# Configuración de Laravel Mix
sed "s/mix.js('resources\/js\/app\.js', 'public\/js')/mix\.copyDirectory('resources\/bpanel4\/assets', 'public\/assets');\n\nmix\.js('resources\/js\/app\.js', 'public\/js')/g" -i webpack.mix.js

REPLACE="])\n \
   \/\/ Añadido por bittacora\/bpanel4-panel\n \
   \.js('resources\/bpanel4\/assets\/js\/ace\.js', 'public\/js')\n \
   \.js('resources\/bpanel4\/assets\/js\/jquery\.bootstrap-duallistbox\.js', 'public\/js')\n \
   \.sass('resources\/bpanel4\/assets\/sass\/app\.scss', 'public\/css')\n \
   \.sass('resources\/bpanel4\/assets\/scss\/ace\.scss', 'public\/css')\n \
   \.sass('resources\/bpanel4\/assets\/scss\/login\.scss', 'public\/css')\n \
   \.css('resources\/bpanel4\/assets\/css\/bootstrap-duallistbox\.min\.css', 'public\/css\/app\.css')\n \
   \.copyDirectory('node_modules\/tinymce', 'public\/vendor\/tinymce')\n \
   \.copyDirectory('node_modules\/flatpickr\/dist', 'public\/vendor\/flatpickr')\n \
   \/\/ Fin de bittacora\/bpanel4-panel\n \
   ;"

sed "s/]);/$REPLACE/g"  -i webpack.mix.js

echo "require('../bpanel4/assets/js/app.js'); // Añadido por bittacora/bpanel4-panel" >> resources/js/app.js

# Publicar contenidos de paquetes
php artisan vendor:publish --tag=bpanel4-panel --force
php artisan vendor:publish --tag=datatables-buttons --force
php artisan vendor:publish --provider="Rappasoft\LaravelLivewireTables\LaravelLivewireTablesServiceProvider" --tag=livewire-tables-config

# Instalación de dependencias de JS
npm install --save bootstrap@^4.0.0 font-awesome@^4.7.0 select2@^4.1.0-rc.0 datatables.net-bs4@^1.10.24 \
    datatables.net-buttons-bs4@^1.7.0 datatables.net-editor@^2.0.2 datatables.net-rowreorder-bs4@^1.2.7 \
    flatpickr alpinejs sweetalert2 popper.js tinymce

npm run dev
npm run dev

# Migraciones
php artisan migrate --seed

# Configurar livewire tables
sed "s/'theme' => 'tailwind',/'theme' => 'bootstrap-4',/g" -i config/livewire-tables.php

echo '                                                                                                   
 @@@*          (@@@@@@@@@@@                                               @@@@               &@@@@  
 @@@*          (@@@      @@@@                                             @@@@              @@@@@@  
 @@@@@@@@@@@   (@@@       @@@%  /@@@@@@@@@    @@@@@@@@@@@    @@@@@@@@@@   @@@@            @@@.@@@@  
 @@@*    @@@@  (@@@@@@@@@@@@@          @@@@   @@@     @@@   @@@@    @@@@  @@@@           @@@  @@@@  
 @@@*    (@@@  (@@@@@@@@@,      ,@@@@@@@@@@   @@@     @@@   @@@@@@@@@@@@  @@@@         @@@@@@@@@@@@@
 @@@*    @@@@  (@@@            @@@@    @@@@   @@@     @@@   @@@@          @@@@         @@@@@@@@@@@@@
 @@@@@@@@@@@   (@@@             @@@@@@@@@@@   @@@     @@@    @@@@@@@@@@.  @@@@                @@@@ '