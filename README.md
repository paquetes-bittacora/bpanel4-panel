# Importante
> :warning:  **Importante**: No usar este paquete directamente. Originalmente era el paquete principal de bPanel4, pero ahora debe usarse bittacora/bpanel4


# bPanel4 Panel

Paquete base que solo registra rutas, etc del panel de control bPanel 4. 

# Instalación

Para instalar este paquete de momento hay que dar algunos pasos adicionales, 
ya que deben publicarse archivos en /public, compilar JS, etc.

Lo primero es añadir lo siguiente al `composer.json` de la raíz del proyecto (por ejemplo, antes de la llave de cierre):

```json
"repositories": [
  {
    "type": "composer",
    "url": "https://composer.bittacora.dev"
  },
  {
    "type": "vcs",
    "url": "https://github.com/laravel-shift/uniquewith-validator.git"
  }
]
```

El primero es nuestro repositorio y el segundo es un parche del validador **uniquewith** que es necesario temporalmente para una de las dependencias del panel, porque todavía no la han actualizado a PHP 8.

Después, hay que hacer unas modificaciones en `webpack.mix.js`. Antes de:

```js
mix.js('resources/js/app.js', 'public/js')
```

hay que añadir:

```js 
mix.copyDirectory('resources/bpanel4/assets', 'public/assets');
```

A continuación de 

```js
.postCss('resources/css/app.css', 'public/css', [
        //
])
```

Hay que añadir

```js
.js('resources/bpanel4/assets/js/ace.js', 'public/js')
.js('resources/bpanel4/assets/js/jquery.bootstrap-duallistbox.js', 'public/js')
.sass('resources/bpanel4/assets/sass/app.scss', 'public/css')
.sass('resources/bpanel4/assets/scss/ace.scss', 'public/css')
.sass('resources/bpanel4/assets/scss/login.scss', 'public/css')
.css('resources/bpanel4/assets/css/bootstrap-duallistbox.min.css', 'public/css/app.css')
.copyDirectory('node_modules/tinymce', 'public/vendor/tinymce')
.copyDirectory('node_modules/flatpickr/dist', 'public/vendor/flatpickr')
```

Tabmién habrá que añadir la siguiente línea en `resources/js/app.js`:

```js
require('../bpanel4/assets/js/app.js');
```

Después, habrá que ejecutar estos comandos (ojo porque al hacer npm run dev te puede salir un mensaje para que lo hagas otra
vez porque necesita instalr dependencias):

```bash
php artisan vendor:publish --tag=bpanel4-panel --force
php artisan vendor:publish --tag=datatables-buttons --force
php artisan vendor:publish --provider="Rappasoft\LaravelLivewireTables\LaravelLivewireTablesServiceProvider" --tag=livewire-tables-config
php artisan vendor:publish --tag=lang
php artisan fortify-translations:install

npm install --save bootstrap@^4.0.0 font-awesome@^4.7.0 select2@^4.1.0-rc.0 datatables.net-bs4@^1.10.24 \
    datatables.net-buttons-bs4@^1.7.0 datatables.net-editor@^2.0.2 datatables.net-rowreorder-bs4@^1.2.7 \
    flatpickr alpinejs sweetalert2 popper.js tinymce

npm run dev
```

Después, ejecutar las migraciones

```
php artisan migrate --seed
```

Ahora, modificar `config/livewire-tables.php` para que use `bootstrap-4`.

Después, ya se podrá acceder a url_de_la_web/bpanel con el usuario debug@bpanel.es y contraseña 'prueba'.

En caso de que algún paquete de algún problema durante la instalación (por ejemplo, no se puede hacer login), revisar
el README del paquete correspondiente.

# Autoinstalador de paquetes 

Ver https://gitlab.com/paquetes-bittacora/autoinstalador-de-paquetes

Para instalar automáticamente los paquetes de bPanel4, habrá que modificar el `composer.json` del proyecto. Hay que modificar el apartado `post-autoload-dump` para añadir la línea `@php artisan bpanel4:install-packages`. Quedaría así: 

```json
"post-autoload-dump": [
    "Illuminate\\Foundation\\ComposerScripts::postAutoloadDump",
    "@php artisan package:discover --ansi",
    "@php artisan bpanel4:install-packages"
],
```

Al hacer este cambio, cada vez que instalemos o actualicemos un paquete de bpanel por composer, se ejecutarán automáticamente sus comandos de instalación.
