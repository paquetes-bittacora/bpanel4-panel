<?php

declare(strict_types=1);

use Bittacora\Bpanel4Panel\Http\Controllers\BpanelController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web', 'auth', 'admin-menu']], function() {
    Route::get('/bpanel', [BpanelController::class, 'index'])->name('bpanel');
});
