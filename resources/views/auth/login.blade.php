@extends('bpanel4::layouts.login-app')

@section('title', 'bPanel')


@section('styles')
    @vite('resources/bpanel4/assets/scss/login.scss')
@endsection


@section('content')
    <div class="main-container container bgc-transparent">

        <div class="main-content minh-100 justify-content-center">
            <div class="p-2 p-md-4">
                <div class="row justify-content-center" id="row-1">
                    <div class="bgc-white shadow radius-1 overflow-hidden col-12 col-lg-6 col-xl-5">

                        <div class="row" id="row-2">

                            <div id="id-col-main" class="col-12 py-lg-5 bgc-white px-0">
                                <!-- you can also use these tab links -->

                                <div class="tab-content tab-sliding border-0 p-0" data-swipe="right">
                                    <div class="tab-pane active show mh-100 px-3 px-lg-0 pb-3" id="id-tab-login">
                                        <!-- show this in desktop -->
                                        <!-- show this in mobile device -->
                                        <div class="text-secondary-m1 my-4 text-center">
                                            <img src="{{ asset('assets/image/logo-login.png') }}" class="img-fluid" alt="bPanel 4">
                                        </div>

                                        <form autocomplete="off" class="form-row mt-4" method="post" action="{{route('login')}}">
                                            @csrf
                                            <div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                                                <div class="d-flex align-items-center input-floating-label  @error('email') text-danger brc-red-m2  @else text-blue
                                                brc-blue-m2 @endif">
                                                    <input
                                                            type="text"
                                                            class="form-control form-control-lg pr-4 shadow-none @error('email') is-invalid @endif"
                                                            id="email"
                                                            name="email"
                                                            value="{{ old('email') }}">
                                                    <i class="fa fa-user text-grey-m2 ml-n4"></i>
                                                    <label class="floating-label text-grey-l1 ml-n3" for="email">
                                                        Email
                                                    </label>
                                                </div>
                                                @error('email')
                                                <div class="clearfix"></div>
                                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 mt-2 mt-md-1">
                                                <div class="d-flex align-items-center input-floating-label text-blue brc-blue-m2">
                                                    <input type="password"
                                                           class="form-control form-control-lg pr-4 shadow-none @error('password') is-invalid @endif"
                                                           id="password"
                                                           name="password">
                                                    <i class="fa fa-key text-grey-m2 ml-n4"></i>
                                                    <label class="floating-label text-grey-l1 ml-n3" for="password">
                                                        Contraseña
                                                    </label>

                                                </div>
                                                @error('password')
                                                <div class="clearfix"></div>
                                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                                                <label class="d-inline-block mt-3 mb-0 text-dark-l1">
                                                    <input type="checkbox" class="mr-1" id="remember" name="remember">
                                                    Recuérdame
                                                </label>
                                                <button type="submit" class="btn btn-primary btn-block px-4 btn-bold mt-2 mb-4">
                                                    Acceder
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- .tab-content -->
                            </div>


                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="d-lg-none my-3 text-white-tp1 text-center">
                        <i class="fa fa-leaf text-success-l3 mr-1 text-110"></i> bPanel 4 © {{(date('Y'))}}
                    </div>
                </div>
            </div>

        </div>
@endsection
