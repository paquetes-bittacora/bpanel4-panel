@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Inicio')

@section('content')
    <div class="row">
        @if(isset($adminMenu[2]))
            @foreach($adminMenu[2]->modules as $module)
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="mt-45 bcard card border-0">
                        <div class="acard card p-0">
                            <div class="card-header card-header-sm bgc-primary-d1 border-0 py-2">
                                <h4 class="card-title text-115 text-white pb-2px">
                                    {{$module->title}}
                                </h4>
                            </div>
                            <div class="card-body pt-2">
                                @foreach($module->actions as $action)
                                    @can($action->parent_key.".".$action->permission)
                                        <a class="btn btn-app btn-secondary btn-xs radius-3 m-1"
                                           id="btn-app-1"
                                           @if (isset($action['route']) AND !empty($action['route'])) href="{{ route($action['route'])}} @endif"
                                           title="{{$action['title']}}">
                                            @if (isset($action['icon']) AND !empty($action['icon']))
                                                <i class="d-block h-4 {{$action['icon']}} text-150"></i>
                                            @endif
                                            {{ $action['title'] }}
                                        </a>
                                    @endcan
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif

        @if(isset($adminMenu[1]))
            @foreach($adminMenu[1]->modules as $module)
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="mt-45 bcard card border-0">
                        <div class="acard card p-0">
                            <div class="card-header card-header-sm bgc-primary-d1 border-0 py-2">
                                <h4 class="card-title text-115 text-white pb-2px">
                                    {{$module->title}}
                                </h4>
                            </div>
                            <div class="card-body pt-2">
                                @foreach($module->actions as $action)
                                    @can($action->parent_key.".".$action->permission)
                                        <a class="btn btn-app btn-secondary btn-xs radius-3 m-1"
                                           id="btn-app-1"
                                           @if (isset($action['route']) AND !empty($action['route'])) href="{{ route($action['route'])}} @endif"
                                           title="{{$action['title']}}">
                                            @if (isset($action['icon']) AND !empty($action['icon']))
                                                <i class="d-block h-4 {{$action['icon']}} text-150"></i>
                                            @endif
                                            {{ $action['title'] }}
                                        </a>
                                    @endcan
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif

        @if(isset($adminMenu[0]))
            @foreach($adminMenu[0]->modules as $module)
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="mt-45 bcard card border-0">
                        <div class="acard card p-0">
                            <div class="card-header card-header-sm bgc-primary-d1 border-0 py-2" >
                                <h4 class="card-title text-115 text-white pb-2px">
                                    {{$module->title}}
                                </h4>
                            </div>
                            <div class="card-body pt-2">
                                @foreach($module->actions as $action)
                                    @can($action->parent_key.".".$action->permission)
                                        <a class="btn btn-app btn-secondary btn-xs radius-3 m-1"
                                           id="btn-app-1"
                                           @if (isset($action['route']) AND !empty($action['route'])) href="{{ route($action['route'])}} @endif"
                                           title="{{$action['title']}}">
                                            @if (isset($action['icon']) AND !empty($action['icon']))
                                                <i class="d-block h-4 {{$action['icon']}} text-150"></i>
                                            @endif
                                            {{ $action['title'] }}
                                        </a>
                                    @endcan
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endsection
