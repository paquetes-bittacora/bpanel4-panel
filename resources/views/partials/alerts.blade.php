@if(Session::has('alert-success'))
    <div class="alert d-flex bgc-green-l4 brc-green-m4 border-1 border-l-0 pl-3 radius-l-0 mb-3" role="alert">
        <div class="position-tl h-102 border-l-4 brc-green mt-n1px"></div>
        <i class="fa fa-check mr-3 text-180 text-green"></i>

        <span class="align-self-center text-green-d2 text-120">
          {{Session::get('alert-success')}}
        </span>
    </div>
    {!! \Illuminate\Support\Facades\Session::forget('alert-success') !!}
@endif


@if(Session::has('alert-danger'))
    <div class="alert d-flex bgc-red-l4 brc-red-m4 border-1 border-l-0 pl-3 radius-l-0 mb-3" role="alert">
        <div class="position-tl h-102 border-l-4 brc-red mt-n1px"></div>
        <i class="fa fa-times mr-3 text-180 text-red"></i>

        <span class="align-self-center text-red-d2 text-120">
          {{Session::get('alert-danger')}}
        </span>
    </div>
    {!! \Illuminate\Support\Facades\Session::forget('alert-danger') !!}
@endif
