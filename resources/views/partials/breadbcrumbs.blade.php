<div class="page-header pb-2">
    <h1 class="page-title text-primary-d2 text-150">
        @if (Route::getCurrentRoute()->action['as']=='bpanel')
            Panel de control
        @else
            <?php list($main,$submain) = explode(".",Route::currentRouteName()) ?>
            {{__($main . '::breadcrumbs.'.$main)}}
            <small class="page-info text-secondary-d2 text-nowrap">
                <i class="fa fa-angle-double-right text-80"></i>
                {{ __($main . '::breadcrumbs.'.$submain) }}
            </small>
        @endif
    </h1>
</div>
