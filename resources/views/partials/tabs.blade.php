
    <x-tabs></x-tabs>
{{-- @elseif (isset($tabs) AND !empty($tabs))
    <div class="mb-3">
        <ul class="mt-10 mb-10 nav nav-justified flex-nowrap nav-tabs nav-tabs-simple nav-tabs-faded nav-tabs-detached radius-0 border-1 bgc-grey-l1 brc-grey-l1 pb-0 shadow-sm"
            role="tablist">
            @foreach ($tabs as $tab)
                @if (isset($tab['permission']))
                    @can($tab['permission'])

                        @if (isset($tab['title']))
                            <?php $title = $tab['title'] ?>
                        @else
                            <?php $title = '-' ?>
                        @endif

                        @if (isset($tab['icon']))
                            <?php $icon = '<i class="' . $tab['icon'] . ' text-180 d-block my-1"></i>'?>
                        @else
                            <?php $icon = ''?>
                        @endif

                        @if (isset($tab['color']))
                            <?php $colorClass = 'btn-h-outline-' . $tab["color"] . ' btn-a-outline-' . $tab['color']?>
                        @else
                            <?php $colorClass = 'btn-h-outline-blue btn-a-outline-blue'?>
                        @endif

                        @if (isset($tab['route']) AND Route::is($tab['route']))
                            <?php $active = 'active'?>
                        @else
                            <?php $active = ''?>
                        @endif

                        @if (isset($tab['isset']))
                            <?php (($root = explode("/", Request::getRequestUri())[1]))?>
                            @if(Route::is($tab['route']) AND isset(${$root}))
                                <?php $route = Route($tab['route'], [$root => ${$root}])?>
                                <li class="nav-item mr-1px">
                                    <a class="btn bgc-white btn-lighter-grey {!! $colorClass !!} py-2 btn-brc-tp border-none border-t-3 radius-0 letter-spacing active"
                                       href="{{$route}}">
                                        {!! $icon !!}
                                        <span class="text-90">{{$title}}</span>
                                    </a>
                                </li>
                            @endif
                        @else
                            <?php $route = Route($tab['route'])?>
                            <li class="nav-item mr-1px">
                                <a class="btn bgc-white btn-lighter-grey {!! $colorClass !!} py-2 btn-brc-tp border-none border-t-3 radius-0 letter-spacing
                            {{$active}}" href="{{$route}}">
                                    {!! $icon !!}
                                    <span class="text-90">{{$title}}</span>
                                </a>
                            </li>
                        @endif
                    @endcan
                @endif
            @endforeach
        </ul>
    </div>
@endif

--}}
