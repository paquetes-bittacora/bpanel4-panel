<div class="sidebar-inner">

    <div class="ace-scroll flex-grow-1 mt-1px" data-ace-scroll="{}">

        <div class="sidebar-section my-2">
            <!-- the shortcut buttons -->
            <div class="sidebar-section-item fadeable-left">
                <div class="fadeinable sidebar-shortcuts-mini"><!-- show this small buttons when collapsed -->
                    <span class="btn btn-success p-0 opacity-1"></span>
                    <span class="btn btn-info p-0 opacity-1"></span>
                    <span class="btn btn-orange p-0 opacity-1"></span>
                    <span class="btn btn-danger p-0 opacity-1"></span>
                </div>

                <div class="fadeable"><!-- show this small buttons when not collapsed -->
                    <div class="sub-arrow"></div>
                    <div>
                        <a class="btn px-25 py-2 text-95 btn-success opacity-1" href="{{route('bpanel')}}"
                           title="Inicio">
                            <i class="fa fa-home f-n-hover"></i>
                        </a>

                        <a class="btn px-25 py-2 text-95 btn-orange opacity-1" title="Mi perfil" href="#">
                            <i class="fa fa-user f-n-hover"></i>
                        </a>

                        <form method="POST" action="{{route('logout')}}" class="d-inline">
                            @csrf
                            <button class="btn px-25 py-2 text-95 btn-danger opacity-1" type="submit"
                                    title="Desconectar">
                                <i class="fa fa-sign-out f-n-hover"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- optional `nav` tag -->
        <nav class="pt-1" aria-label="Main">
            <ul class="nav flex-column has-active-border">
                <x-admin-menu-sidebar/>
{{--                @foreach (Config::get('modules') as $group)--}}
{{--                    @foreach($group['items'] as $module)--}}

{{--                    <li class="nav-item">--}}
{{--                        <a href="@if (isset($group['route']) AND !empty($group['route'])){{route($group['route'])}}@else#@endif"--}}
{{--                           class="nav-link @if (isset($group['items']) AND !empty($group['items'])) dropdown-toggle collapse --}}{{-- si submenu collapse--}}{{-- @endif">--}}
{{--                            @if (isset($group['icon']) AND !empty($group['icon']))--}}
{{--                                <i class="nav-icon {{$group['icon']}}"></i>--}}
{{--                            @endif--}}
{{--                            <span class="nav-text fadeable--}}{{--si toplevel fadeable --}}{{--">{{$group['title']}}</span>--}}

{{--                            @if (isset($group['badge']) AND !empty($group['badge']))--}}
{{--                                <span class="badge @if($group['badge']['class']){{$group['badge']['class']}}@endif--}}
{{--                                @if($group['badge']['tooltip']){{$group['badge']['tooltip']}}@endif">--}}
{{--                                      {{$group['badge']['content']}}--}}
{{--                                  </span>--}}
{{--                            @endif--}}
{{--                            @if (isset($group['items']) AND !empty($group['items']))--}}
{{--                                <b class="caret fa fa-angle-left rt-n90"></b>--}}
{{--                            @endif--}}
{{--                            --}}{{-- if children--}}
{{--                        <!-- or you can use custom icons. first add `d-style` to 'A' -->--}}
{{--                            <!----}}
{{--                                <b class="caret d-n-collapsed fa fa-minus text-80"></b>--}}
{{--                                <b class="caret d-collapsed fa fa-plus text-80"></b>--}}
{{--                            -->--}}
{{--                            /if--}}
{{--                        </a>--}}

{{--                        @if (isset($group['items']) AND !empty($group['items']))--}}
{{--                            <div--}}
{{--                                class="hideable submenu collapse--}}{{-- #if toplevel}}hideable --}}{{-- {{/if}} submenu collapse{{#if submenuClassName}} {{submenuClassName}}{{/if}} --}}{{--">--}}
{{--                                <ul class="submenu-inner">--}}
{{--                                    @foreach ($group['items'] as $module)--}}
{{--                                        <li>--}}
{{--                                            {{ $module['title'] }}--}}
{{--                                            <ul>--}}
{{--                                                @foreach($module['actions'] as $action)--}}
{{--                                                    <li><i class="{{$action['icon']}}"></i> {{ $action['title'] }}</li>--}}
{{--                                                @endforeach--}}
{{--                                            </ul>--}}
{{--                                        </li>--}}
{{--                                        @if (isset($module['permission']))--}}
{{--                                            @can($module['permission'])--}}
{{--                                                <li class="nav-item">--}}
{{--                                                    <a href="{{route($module['route'])}}"--}}
{{--                                                       title="{{$module['title']}}"--}}
{{--                                                       class="nav-link">--}}
{{--                                            <span class="nav-text">--}}
{{--                                            <span>--}}
{{--                                                @if (isset($module['icon']) AND !empty($module['icon']))--}}
{{--                                                    <i class="{{$module['icon']}}"></i>--}}
{{--                                                @endif--}}
{{--                                                {{$module['title']}}--}}
{{--                                            </span>--}}
{{--                                            </span>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                            @endcan--}}
{{--                                        @endif--}}
{{--                                    @endforeach--}}
{{--                                    --}}{{----}}
{{--                                    {{#each children}}--}}
{{--                                    {{> layout/_commons/sidebar-item}}--}}
{{--                                    {{/each}}--}}
{{--                                </ul>--}}
{{--                            </div>--}}

{{--                            <b class="sub-arrow"></b>--}}
{{--                            --}}{{--@elseif (toplevel}}--}}
{{--                            <b class="sub-arrow"></b>--}}
{{--                        @endif--}}
{{--                    </li>--}}
{{--                @endforeach--}}
{{--                @endforeach--}}
            </ul>
        </nav>

    </div><!-- /.ace-scroll -->


</div>
