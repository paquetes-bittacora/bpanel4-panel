{{--<nav class="navbar navbar-sm navbar-expand-lg navbar-fixed navbar-blue">--}}
{{--    <div class="navbar-inner brc-grey-l2 shadow-md">--}}

{{--        <!-- this button collapses/expands sidebar in desktop mode -->--}}
{{--        <button type="button" class="btn btn-burger align-self-center d-none d-xl-flex mx-2" data-toggle="sidebar" data-target="#sidebar" aria-controls="sidebar"--}}
{{--                aria-expanded="true" aria-label="Toggle sidebar">--}}
{{--            <span class="bars"></span>--}}
{{--        </button>--}}

{{--        <div class="d-flex h-100 align-items-center justify-content-xl-between">--}}
{{--            <!-- this button shows/hides sidebar in mobile mode -->--}}
{{--            <button type="button" class="btn btn-burger static burger-arrowed collapsed d-flex d-xl-none ml-2 bgc-h-white-l31" data-toggle-mobile="sidebar"--}}
{{--                    data-target="#sidebar" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle sidebar">--}}
{{--                <span class="bars text-white"></span>--}}
{{--            </button>--}}

{{--            <a class="navbar-brand ml-2 text-white" href="{{route('bpanel')}}">--}}
{{--                {{ config('app.name') }}--}}
{{--            </a>--}}
{{--        </div>--}}


{{--        <!-- .navbar-menu toggler -->--}}
{{--        <button class="navbar-toggler mx-1 p-25" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false"--}}
{{--                aria-label="Toggle navbar menu">--}}
{{--            <i class="fa fa-user text-white"></i>--}}
{{--        </button>--}}


{{--        <div class="ml-auto mr-lg-2 navbar-menu collapse navbar-collapse navbar-backdrop" id="navbarMenu">--}}
{{--            <div class="navbar-nav">--}}
{{--                <ul class="nav nav-compact-2">--}}
{{--                    <li class="nav-item dropdown order-first order-lg-last">--}}
{{--                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownNavMenuButton" role="button" aria-haspopup="true"--}}
{{--                           aria-expanded="false">--}}
{{--                            <span class="d-inline-block d-lg-none d-xl-inline-block">--}}
{{--                                <span class="text-90" id="id-user-welcome">Bienvenido,</span>--}}
{{--                                <span class="nav-user-name">{{Auth::user()->name}}</span>--}}
{{--                            </span>--}}

{{--                            <i class="caret fa fa-angle-down d-none d-xl-block"></i>--}}
{{--                            <i class="caret fa fa-angle-left d-block d-lg-none"></i>--}}
{{--                        </a>--}}

{{--                        <div class="dropdown-menu dropdown-caret dropdown-menu-right dropdown-animated brc-primary-m3 py-1" aria-labelledby="dropdownNavMenuButton">--}}
{{--                            <div class="d-none d-lg-block d-xl-none">--}}
{{--                                <div class="dropdown-header">--}}
{{--                                    Welcome, Jason--}}
{{--                                </div>--}}
{{--                                <div class="dropdown-divider"></div>--}}
{{--                            </div>--}}

{{--                            <a class="mt-1 dropdown-item btn btn-outline-grey bgc-h-primary-l3 btn-h-light-primary btn-a-light-primary" href="#">--}}
{{--                                <i class="fa fa-user text-primary-m1 text-105 mr-1"></i>--}}
{{--                                Perfil--}}
{{--                            </a>--}}

{{--                            <div class="dropdown-divider brc-primary-l2"></div>--}}

{{--                            <form method="post" action="{{route('logout')}}" class="d-inline">--}}
{{--                                @csrf--}}
{{--                            <button type="submit" class="dropdown-item btn btn-outline-grey bgc-h-secondary-l3 btn-h-light-secondary btn-a-light-secondary">--}}
{{--                                <i class="fa fa-power-off text-warning-d1 text-105 mr-1"></i>--}}
{{--                                Desconectar--}}
{{--                            </button>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </li><!-- /.nav-item:last -->--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div><!-- .navbar-menu -->--}}


{{--    </div><!-- .navbar-inner -->--}}
{{--</nav>--}}


<nav class="navbar navbar-expand-lg navbar-fixed navbar-blue">
    <div class="navbar-inner">

        <div class="navbar-intro justify-content-xl-between">

            <button type="button" class="btn btn-burger burger-arrowed static collapsed ml-2 d-flex d-xl-none" data-toggle-mobile="sidebar" data-target="#sidebar" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle sidebar">
                <span class="bars"></span>
            </button><!-- mobile sidebar toggler button -->

            <a class="navbar-brand text-white" href="{{route('bpanel')}}">
                <span>{{ config('app.name') }}</span>
            </a><!-- /.navbar-brand -->

            <button type="button" class="btn btn-burger mr-2 d-none d-xl-flex" data-toggle="sidebar" data-target="#sidebar" aria-controls="sidebar" aria-expanded="true" aria-label="Toggle sidebar">
                <span class="bars"></span>
            </button><!-- sidebar toggler button -->

        </div><!-- /.navbar-intro -->


        <div class="navbar-content">
            <button class="navbar-toggler py-2" type="button" data-toggle="collapse" data-target="#navbarSearch" aria-controls="navbarSearch" aria-expanded="false" aria-label="Toggle navbar search">
                <i class="fa fa-search text-white text-90 py-1"></i>
            </button><!-- mobile #navbarSearch toggler -->

            <div class="collapse navbar-collapse navbar-backdrop" id="navbarSearch">
                <form class="d-flex align-items-center ml-lg-4 py-1" data-submit="dismiss">
                    <i class="fa fa-search text-white d-none d-lg-block pos-rel"></i>
                    <input type="text" class="navbar-input mx-3 flex-grow-1 mx-md-auto pr-1 pl-lg-4 ml-lg-n3 py-2 autofocus" placeholder="BUSCAR ..." aria-label="Search" />
                </form>
            </div>
        </div><!-- .navbar-content -->


        <!-- mobile #navbarMenu toggler button -->
        <button class="navbar-toggler ml-1 mr-2 px-1" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navbar menu">
            <span class="pos-rel">
                  <img class="border-2 brc-white-tp1 radius-round" width="36" src="/assets/image/avatar/avatar6.jpg" alt="Jason's Photo">
                  <span class="bgc-warning radius-round border-2 brc-white p-1 position-tr mr-n1px mt-n1px"></span>
            </span>
        </button>


        <div class="navbar-menu collapse navbar-collapse navbar-backdrop" id="navbarMenu">

            <div class="navbar-nav">
                <ul class="nav">
                    <li class="nav-item dropdown order-first order-lg-last">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="d-inline-block d-lg-none d-xl-inline-block">
                              <span class="text-90" id="id-user-welcome">Bienvenido,</span>
                    <span class="nav-user-name">{{Auth::user()->name}}</span>
                    </span>

                            <i class="caret fa fa-angle-down d-none d-xl-block"></i>
                            <i class="caret fa fa-angle-left d-block d-lg-none"></i>
                        </a>

                        <div class="dropdown-menu dropdown-caret dropdown-menu-right dropdown-animated brc-primary-m3 py-1">
                            <div class="d-none d-lg-block d-xl-none">
                                <div class="dropdown-header">
                                    Welcome, Jason
                                </div>
                                <div class="dropdown-divider"></div>
                            </div>
                            <a class="mt-1 dropdown-item btn btn-outline-grey bgc-h-primary-l3 btn-h-light-primary btn-a-light-primary" href="html/page-profile.html">
                                <i class="fa fa-user text-primary-m1 text-105 mr-1"></i>
                                Perfil
                            </a>
                            <div class="dropdown-divider brc-primary-l2"></div>

                            <form method="post" action="{{route('logout')}}" class="d-inline">
                                @csrf
                                <button type="submit" class="dropdown-item btn btn-outline-grey bgc-h-secondary-l3 btn-h-light-secondary btn-a-light-secondary">
                                    <i class="fa fa-power-off text-warning-d1 text-105 mr-1"></i>
                                    Desconectar
                                </button>
                            </form>
                        </div>
                    </li><!-- /.nav-item:last -->

                </ul><!-- /.navbar-nav menu -->
            </div><!-- /.navbar-nav -->

        </div><!-- /#navbarMenu -->


    </div><!-- /.navbar-inner -->
</nav>
