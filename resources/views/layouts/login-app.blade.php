<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    <base href="../"/>

    <title>bPanel 4 | @yield('title')</title>

    <!-- include common vendor stylesheets & fontawesome -->
    @vite('resources/bpanel4/assets/sass/app.scss')
    @vite('resources/bpanel4/assets/scss/ace.scss')

    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{asset('assets/favicon.png')}}"/>

    @yield('styles')
</head>

<body>
<div class="body-container">
    @yield('content')
</div>

@yield('partials.footer')
</body>
</html>
