<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title') - bPanel 4</title>

    <!-- include common vendor stylesheets & fontawesome -->
    @vite('resources/bpanel4/assets/sass/app.scss')
    @vite('resources/bpanel4/assets/scss/ace.scss')
    @vite('resources/bpanel4/assets/vendor/fontawesome-pro-5.15.4/css/all.css')


    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{asset('assets/favicon.png')}}"/>

{{--    <script src="{{asset ('assets/js/ace.js')}}"></script>--}}
    @stack('styles')
    @livewireStyles
</head>

<body>

<div class="body-container">

    {{-- Navbar --}}
    @include('bpanel4::partials.navbar')

    <div class="main-container bgc-white">

        <!-- Sidebar -->
        <div id="sidebar" class="sidebar sidebar-fixed expandable sidebar-light" data-backdrop="true" data-dismiss="true" data-swipe="true">
            @include('bpanel4::partials.sidebar')
        </div>

        <!-- Main Content -->
        <div class="main-content">
            <!-- Page Content -->
            <div class="page-content">

                @include('bpanel4::partials.breadbcrumbs')

                @include('bpanel4::partials.tabs')

                @include('bpanel4::partials.alerts')

                @yield('content')
            </div>
        </div>
    </div>
</div>

@livewireScripts
@include('bpanel4::partials.footer')
@stack('scripts')

<script>
  /* @todo Javi Esto habrá que moverlo a otro sitio */
  window.addEventListener('load', function(event) {
      tinymce.init({
        selector:'.tinymce-editor-bp4',
        plugins: "lists advlist table image link anchor media",
        toolbar: [
          "styleselect | fontsizeselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formats | forecolor backcolor | numlist bullist | table | image link media | toc | hr"
        ],
        skin: false,
        content_css: '{{ Vite::asset('node_modules/tinymce/skins/content/default/content.css') }}',
        automatic_uploads: true,
        image_advtab: true,
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        images_upload_handler: example_image_upload_handler,
        block_unsupported_drop: false,
        image_class_list: [
          {title: 'Ninguno', value: ''},
          {title: 'Flotar a la derecha', value: 'tinymce_float_right_image'},
          {title: 'Flotar a la izquierda', value: 'tinymce_float_left_image'}
        ],
        /* without images_upload_url set, Upload tab won't show up*/
          @if(Route::has('multimedia.singleuploadstore'))
          images_upload_url: "{{route('multimedia.singleuploadstore')}}",
          @endif
          language: "es",
        min_height: 500,
        file_picker_callback: function(callback, value, meta) {
          window.livewire.emit('tinyMceOpenModal');
          window.addEventListener('addUrlFromMediaLibraryToTinyMce', function(event){
            callback(event.detail.url);
          });
        },
      });
    });


  @if(Route::has('multimedia.singleuploadstore'))
  function example_image_upload_handler (blobInfo, success, failure, progress) {
    var xhr, formData;

    xhr = new XMLHttpRequest();
    xhr.open('POST', "{{route('multimedia.singleuploadstore')}}");
    xhr.withCredentials = false;
    var token = '{{ csrf_token() }}';
    xhr.setRequestHeader("X-CSRF-Token", token);


    xhr.upload.onprogress = function (e) {
      progress(e.loaded / e.total * 100);
    };

    xhr.onload = function() {
      var json;

      if (xhr.status === 403) {
        failure('HTTP Error: ' + xhr.status, { remove: true });
        return;
      }

      if (xhr.status < 200 || xhr.status >= 300) {
        failure('HTTP Error: ' + xhr.status);
        return;
      }


      json = JSON.parse(xhr.responseText);


      if (!json || typeof json.location != 'string') {
        failure('Invalid JSON: ' + xhr.responseText);
        return;
      }

      success(json.location);
    };

    xhr.onerror = function () {
      failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
    };

    formData = new FormData();
    formData.append('file', blobInfo.blob(), blobInfo.filename());
    xhr.send(formData);

    window.livewire.emit('refreshMultimediaImagesLibrary');
  };
  @else
  function example_image_upload_handler (blobInfo, success, failure, progress) {}
  @endif

</script>
</body>
</html>
