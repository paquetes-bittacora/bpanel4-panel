// bPanel4

import './register-jquery.js'

import bootstrap from 'bootstrap'

import flatpickr from 'flatpickr';
import '/node_modules/flatpickr/dist/flatpickr.css';

import './register-select2.js'

import '/node_modules/bootstrap-duallistbox/dist/jquery.bootstrap-duallistbox.js';

import '../scss/dual-list-box.scss'

import tinymce from "tinymce";
import '/node_modules/tinymce/themes/silver/theme';
import '/node_modules/tinymce/models/dom/model';
import '/node_modules/tinymce/plugins/lists/plugin';
import '/node_modules/tinymce/plugins/table/plugin';
import '/node_modules/tinymce/plugins/image/plugin';
import '/node_modules/tinymce/plugins/anchor/plugin';
import '/node_modules/tinymce/plugins/media/plugin';
import '/node_modules/tinymce/plugins/link/plugin';
import '/node_modules/tinymce/plugins/advlist/plugin';
import '/node_modules/tinymce/icons/default/icons';
import '/node_modules/tinymce-i18n/langs6/es.js';
import '/node_modules/tinymce/skins/ui/oxide/skin.css';
import '/node_modules/bootstrap-fileinput/js/fileinput.js'
import '/node_modules/bootstrap-fileinput/css/fileinput.css'

// Sweet alert
import Swal from 'sweetalert2'
window.Swal = Swal;
