<!--- BEGIN HEADER -->
# Changelog

All notable changes to this project will be documented in this file.
<!--- END HEADER -->

## [0.0.1](https://gitlab.com/paquetes-bittacora/bpanel4-panel/compare/91f9bfea9e3670868377f9a06fff5ca0593605bc...v0.0.1) (2021-11-02)

---

# Changelog

All notable changes to `bpanel4-panel` will be documented in this file.

## 1.0.0 - 202X-XX-XX

- initial release
